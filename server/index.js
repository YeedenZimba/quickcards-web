const express = require("express");
const app = express();
const cors = require("cors");
const pool = require("./db");
const bcrypt = require("bcryptjs");
const session = require('express-session');
const Redis = require('ioredis');
const cookieParser = require('cookie-parser');
const nodemailer = require("nodemailer");
const RedisStore = require("connect-redis").default;


const PORT = process.env.PORT || 5000;
const origin = process.env.NODE_ENV === "production" ? 'https://quickcards.vercel.app' : 'http://localhost:3000';
const adminPassword = process.env.ADMIN_PASS || "12345678";
const appUrl = process.env.APP_URL

const corsOptions ={
  origin: origin, // use your actual domain name (or localhost), using * is not recommended
  methods: ['GET', 'POST', 'PUT', 'DELETE', 'PATCH', 'HEAD', 'OPTIONS'],
  allowedHeaders: ['Content-Type', 'Origin', 'X-Requested-With', 'Accept', 'x-client-key', 'x-client-token', 'x-client-secret', 'Authorization'],
  credentials: true
}

//middlewares
app.use(cookieParser());
app.use(cors(corsOptions));
app.use(express.json()); //parses req.body

if (process.env.NODE_ENV === "production"){
  const redisClient = new Redis(process.env.REDIS_URL);
  redisClient.on('error', err => console.log('Redis Server Error', err));

  app.use(session({
    store: new RedisStore({client: redisClient}),
    secret: process.env.SESSION_SECRET,
    name: 'userSessionID',
    resave: false,
    saveUninitialized: false,
    cookie: { 
      maxAge: 1000 * 60 * 60 * 24, 
      secure: 'auto',
      sameSite: 'none',
      httpOnly: true
    },
    proxy: true
  }));
}else{
  app.use(session({
    secret: 'un perro',
    name: 'userSessionID',
    resave: false,
    saveUninitialized: false,
    cookie: { 
      maxAge: 1000 * 60 * 60 * 24,
    }
  }));
}

//functions
function validName(name){
  return /^[\w\s]{1,30}$/.test(name);
}

function validNote(note){
  return note.length <= 255;
}

function validEmail(email){
  return /.+@.+/.test(email);
}

function validPassword(password){
  return /.{8,}/.test(password);
}

function arrayMove(arr, fromIndex, toIndex) {
  var element = arr[fromIndex];
  arr.splice(fromIndex, 1);
  arr.splice(toIndex, 0, element);
}

function diff_minutes(dt2, dt1) {
  const diff = dt2.getTime() - dt1.getTime();

  return (diff / 60000);
}

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min) + min); // The maximum is exclusive and the minimum is inclusive
}

async function sendEmail(to, subject, text) {
  let transporter = nodemailer.createTransport({
    host: "smtp.zoho.com",
    secure: true,
    port: 465,
    auth: {
      user: process.env.SMTP_USER,
      pass: process.env.SMTP_PASS,
    },
  });


  await transporter.sendMail({
    from: process.env.SMTP_USER,
    to: to,
    subject: subject,
    text: text
  });
}

//custom middleware functions
async function restrictEmailUse(req, res, next) {
  const { email } = req.body;

  try{
    const users = await pool.query(
      "SELECT * FROM users WHERE email = $1",
      [email]
    );

    if (users.rows.length > 0){
      res.status(400).json({ input: "email", feedback: "Email in use" });
      return
    }

    next()
  }
  catch (err){
    res.status(500).send(err.message);
  };
}


async function validateUserInfo(req, res, next){
  const { name, email, password, confirmPassword } = req.body;

  if (req.path === "/sign-in"){
    if (!email){
      return res.status(400).json({ input: "email", feedback: "Missing email" });
    } else if (!validEmail(email)){
      return res.status(400).json({ input: "email", feedback: "Invalid email" });
    } else if (!password){
      return res.status(400).json({ input: "password", feedback: "Missing password" });
    }
  } else if (req.path === "/admin-sign-in"){
    if (!password){
      return res.status(400).json({ input: "password", feedback: "Missing password" });
    }
  } else if(req.path === "/sign-up-request"){
    if (!name){
      return res.status(400).json({ input: "name", feedback: "Missing name" });
    } else if (!validName(name)){
      return res.status(400).json({ input: "name", feedback: "Invalid Name. Should have a max of 30 aplhanumeric characters with spaces" });
    } else if (!email){
      return res.status(400).json({ input: "email", feedback: "Missing email" });
    } else if (!validEmail(email)){
      return res.status(400).json({ input: "email", feedback: "Invalid email" });
    } else if (!password){
      return res.status(400).json({ input: "password", feedback: "Missing password" });
    } else if (!validPassword(password)){
      return res.status(400).json({ input: "password", feedback: "Invalid password. Must have atleast 8 characters" });
    } else if (password !== confirmPassword){
      return res.status(400).json({ input: "confirmPassword", feedback: "Incorrect password" });
    }   
  } else if(req.path === "/forgot-password-request"){
    if (!email){
      return res.status(400).json({ input: "email", feedback: "Missing email" });
    } else if (!validEmail(email)){
      return res.status(400).json({ input: "email", feedback: "Invalid email" });
    } else if (!password){
      return res.status(400).json({ input: "password", feedback: "Missing password" });
    } else if (!validPassword(password)){
      return res.status(400).json({ input: "password", feedback: "Invalid password. Must have atleast 8 characters" });
    } else if (password !== confirmPassword){
      return res.status(400).json({ input: "confirmPassword", feedback: "Incorrect password" });
    }   
  } else if(req.path === "/name-validate-only"){
    if (!name){
      return res.status(400).json({ input: "name", feedback: "Missing name" });
    } else if (!validName(name)){
      return res.status(400).json({ input: "name", feedback: "Invalid Name. Should have a max of 30 aplhanumeric characters with spaces" });
    }
  } else if(req.path === "/email-validate-only"){
    if (!email){
      return res.status(400).json({ input: "email", feedback: "Missing email" });
    } else if (!validEmail(email)){
      return res.status(400).json({ input: "email", feedback: "Invalid email" });
    }
  } else if(req.path === "/password-validate-only"){
    if (!password){
      return res.status(400).json({ input: "newPassword", feedback: "Missing password" });
    } else if (!validPassword(password)){
      return res.status(400).json({ input: "newPassword", feedback: "Invalid password. Must have atleast 8 characters" });
    } else if (password !== confirmPassword){
      return res.status(400).json({ input: "confirmPassword", feedback: "Incorrect password" });
    }   
  }
  
  next();
}


async function validateDeck(req, res, next){
  try{
    const uid = req.session.user.uid;
    const { deckName, theme, did } = req.body;

    if (!deckName){
      return res.status(400).json({ input: "deckName", feedback: "Missing name" });
    } else if (!validName(deckName)){
      return res.status(400).json({ input: "deckName", feedback: "Invalid deck name. Name should have a max of 30 aplhanumeric characters with spaces" });
    }
    
    const userDecks = await pool.query(
      "SELECT decks FROM users WHERE uid = $1",
      [uid]
    );
    const decks = await pool.query(
      "SELECT did, name FROM decks WHERE did = ANY($1)",
      [userDecks.rows[0].decks]
    );
    let nameUsed = 0;
    decks.rows.forEach(row => {
      if (row.name === deckName){
        if (req.method === "POST"){
          nameUsed += 1;
        } else if (req.method === "PUT" && row.did != did){
          nameUsed += 1;
        }
      }
    })
    
    if (nameUsed > 0){
      return res.status(400).json({ input: "deckName", feedback: "Deck name already in use. Try another one" });
    } else if (!theme){
      return res.status(400).json({ input: "theme", feedback: "Invalid deck theme" });
    }
  }
  catch (err){
    return res.status(500).send(err.message);
  }

  next();
}


async function validateCard(req, res, next){
  try{
    const uid = req.session.user.uid;
    const { title, order, note, did, cid } = req.body;

    if (!title){
      return res.status(400).json({ input: "title", feedback: "Missing title" });
    } else if (!validName(title)){
      return res.status(400).json({ input: "title", feedback: "Invalid card title. Name should have a max of 30 aplhanumeric characters with spaces" });
    }
    
    const deckCards = await pool.query(
      "SELECT cards FROM decks WHERE did = $1",
      [did]
    );
    const cards = await pool.query(
      "SELECT cid, title FROM cards WHERE cid = ANY($1)",
      [deckCards.rows[0].cards]
    );
    let titleUsed = 0;
    cards.rows.forEach(row => {
      if (row.title === title){
        if (req.method === "POST"){
          titleUsed += 1;
        } else if (req.method === "PUT" && row.cid != cid){
          titleUsed += 1;
        }
      }
    })
    
    if (titleUsed > 0){
      return res.status(400).json({ input: "title", feedback: "Card title already in use. Try another one" });
    } else if (!note){
      return res.status(400).json({ input: "note", feedback: "Note can't be empty" });
    }  else if (!validNote(note)){
      return res.status(400).json({ input: "note", feedback: "Note should have a maximum of 255 characters" });
    } else if (!order){
      return res.status(400).json({ input: "order", feedback: "Invalid card order" });
    }
  }
  catch (err){
    return res.status(500).send(err.message);
  }

  next();
}


async function authuenticate(req, res, next) {
  const { email, password } = req.body;

  try{
    const users = await pool.query(
      "SELECT * from users WHERE email = $1",
      [email]
    );
      
    if (users.rows.length === 0){
      res.status(401).json({ input: "password", feedback: "Invalid credential" });
    }
    else{
      const hash = users.rows[0].password;
      if (bcrypt.compareSync(password, hash)){
        //success
        next()
      }
      else{
        res.status(401).json({ input: "password", feedback: "Invalid credential" });
      }
    }
  }
  catch (err){
    res.status(500).send(err.message);
  }
}


async function authorize(req, res, next) {
  if (req.session.user){
    next();
  }
  else{
    res.status(403).send("Failed to authorize!");
  }
}


async function adminAuthorize(req, res, next) {
  if (req.session.admin){
    next();
  }
  else{
    res.status(403).send("Failed to authorize!");
  }
}


//COOKIES
app.post("/home-cookie", (req, res) => {
  res.cookie('home', req.body, {maxAge: 1000 * 60 * 60 * 24, secure: 'auto', sameSite: 'none', httpOnly: true});
  res.send("Cookie created successfully");
});

app.get("/home-cookie", (req, res) => {
  if (req.cookies.home){
    res.status(200).json(req.cookies.home);
  } else{
    res.status(404).send("Failed to get 'home' cookie");
  }
});

app.post("/cards-cookie", (req, res) => {
  res.cookie('cards', req.body, {maxAge: 1000 * 60 * 60 * 24, secure: 'auto', sameSite: 'none', httpOnly: true});
  res.send("Cookie created successfully");
});

app.get("/cards-cookie", (req, res) => {
  if (req.cookies.cards){
    res.status(200).json(req.cookies.cards);
  } else{
    res.status(404).send("Failed to get 'cardSize' cookie");
  }
});


//ROUTES
app.post("/sign-up-request", validateUserInfo, restrictEmailUse, async (req, res) => {
  try{
    const { name, email, password } = req.body;

    var requestToken = getRandomInt(1000000, 9999999).toString();

    await pool.query("BEGIN");
    try{
      const newRequests = await pool.query(
        "INSERT INTO sign_up_requests(user_name, user_email, user_password, token)\
        VALUES ($1, $2, $3, $4)\
        RETURNING *",
        [name, email, password, requestToken]
      );
      const request = newRequests.rows[0]

      const link = `${origin}/verify-sign-up/${request.id}/${request.token}`;
      const body = `Visit this page to confirm sign up ${link}`;
      await sendEmail(email, 'Verification email', body);
      await pool.query("COMMIT");
      return res.status(200).send("Sign up request set up successful");
    } catch(err){
      await pool.query("ROLLBACK");
      return res.status(500).send(err.message);
    }
  }
  catch(err){
    res.status(500).send(err.message);
  }
});


app.post("/forgot-password-request", validateUserInfo, async (req, res) => {
  try{
    const { email, password } = req.body;

    var requestToken = getRandomInt(1000000, 9999999).toString();

    await pool.query("BEGIN");
    try{
      const newRequests = await pool.query(
        "INSERT INTO forgot_password_requests(user_email, user_password, token)\
        VALUES ($1, $2, $3)\
        RETURNING *",
        [email, password, requestToken]
      );
      const request = newRequests.rows[0]

      const link = `${origin}/verify-forgot-password/${request.id}/${request.token}`;
      const body = `Visit this page to change your password ${link}`;
      await sendEmail(email, 'Verification email', body);
      await pool.query("COMMIT");
      return res.status(200).send("Forgot password request set up successful");
    } catch(err){
      await pool.query("ROLLBACK");
      console.log(err.message);
      return res.status(500).send(err.message);
    }
  }
  catch(err){
    res.status(500).send(err.message);
  }
});


app.post("/email-change-request", authuenticate, async (req, res) => {
  try{
    const { newEmail, uid } = req.body;

    var requestToken = getRandomInt(1000000, 9999999).toString();

    await pool.query("BEGIN");
    try{
      const newRequests = await pool.query(
        "INSERT INTO email_change_requests(user_uid, user_email, token)\
        VALUES ($1, $2, $3)\
        RETURNING *",
        [uid, newEmail, requestToken]
      );
      const request = newRequests.rows[0]

      const link = `${origin}/verify-email-change/${request.id}/${request.token}`;
      const body = `Visit this page to change your email ${link}`;
      await sendEmail(newEmail, 'Verification email', body);
      await pool.query("COMMIT");
      return res.status(200).send("Email change request set up successful");
    } catch(err){
      await pool.query("ROLLBACK");
      return res.status(500).send(err.message);
    }
  }
  catch(err){
    res.status(500).send(err.message);
  }
});


app.post("/zoho-email", async (req, res) => {
  var {user, pass, to, subject, html} = req.body;

  let transporter = nodemailer.createTransport({
    host: "smtp.zoho.com",
    secure: true,
    port: 465,
    auth: {
      user: user,
      pass: pass,
    },
  });

  try{
    await transporter.sendMail({
      from: user,
      to: to,
      subject: subject,
      html: html
    });

    res.status(200).send("The email was sent successfully");
  }catch(err){
    res.status(500).send(err.message);
  }
})


app.post("/deck", authorize, validateDeck, async (req, res) => {
  try{
    const { uid } = req.session.user;
    const { deckName, theme } = req.body;

    //using psql transactions to ensure data consistency among tables
    await pool.query('BEGIN');
    try{
      const newDeck = await pool.query(
        "INSERT INTO decks(name, theme, cards, owner)\
        VALUES ($1, $2, $3, $4)\
        RETURNING *",
        [deckName, theme, [], uid]
      );

      if (newDeck.length === 0) throw new Error("failed to create!");

      await pool.query(
        "UPDATE users\
        SET decks = ARRAY_APPEND(decks, $1)\
        WHERE uid = $2",
        [newDeck.rows[0].did, uid]
      );
      await pool.query('COMMIT');
      return res.status(200).json(newDeck.rows[0]); 
    } catch (err){
      await pool.query('ROLLBACK');
      return res.status(500).send(err.message);
    }
  }
  catch(err){
    res.status(500).send(err.message);
  }
});


app.post("/card", authorize, validateCard, async (req, res) => {
  try{
    const { uid } = req.session.user;
    const { title, note, did } = req.body;
    let order = req.body.order;

    await pool.query('BEGIN');
    try{
      const newCard = await pool.query(
        "INSERT INTO cards(title, note, owner)\
        VALUES ($1, $2, $3)\
        RETURNING *",
        [title, note, did]
      );

      if (newCard.length === 0) throw new Error("failed to create!");

      const decks = await pool.query(
        "SELECT cards FROM decks\
        WHERE did = $1",
        [did]
      );
      let cards = decks.rows[0].cards;
      if (order === -1){
        order = cards.length;
      } else{
        order -= 1;
      }

      cards.splice(order, 0, newCard.rows[0].cid);
      await pool.query(
        "UPDATE decks\
        SET cards = $1\
        WHERE did = $2 AND owner = $3",
        [cards, did, uid]
      );

      await pool.query('COMMIT');
      return res.status(200).json(newCard.rows[0]);
    } catch (err){
      await pool.query('ROLLBACK');
      return res.status(500).send(err.message);
    }
  }
  catch(err){
    res.status(500).send(err.message);
  }
});


app.put("/name", authuenticate, async(req, res) => {
  const {name, uid} = req.body;

  try{
    const users = await pool.query(
      "UPDATE users SET name = $1 WHERE uid = $2",
      [name, uid]
    );
    
    res.status(200).send("Name changed");
  }
  catch (err){
    res.status(500).send(err.message);
  }
});


app.put("/password", authuenticate, async(req, res) => {
  const {newPassword, uid} = req.body;
  var salt = bcrypt.genSaltSync(10);
  var hash = bcrypt.hashSync(newPassword, salt);

  try{
    const users = await pool.query(
      "UPDATE users SET password = $1 WHERE uid = $2",
      [hash, uid]
    );
    
    res.status(200).send("Password changed");
  }
  catch (err){
    res.status(500).send(err.message);
  }
});


app.put("/deck", authorize, validateDeck, async(req, res) => {
  try{
    const { uid } = req.session.user;
    const { did, deckName, theme } = req.body;

    const decks = await pool.query(
      "UPDATE decks SET name = $1, theme = $2 WHERE did = $3 AND owner = $4 RETURNING *",
      [deckName, theme, did, uid]
    );
    
    res.status(200).json(decks.rows[0]);
  }
  catch (err){
    res.status(500).send(err.message);
  }
});


app.put("/card", authorize, validateCard, async(req, res) => {
  try{
    const { uid } = req.session.user;
    const { title, cid, did, note } = req.body;
    let order = req.body.order;
    const decks = await pool.query(
      "SELECT cards FROM decks\
      WHERE did = $1 AND owner = $2",
      [did, uid]
    );
    let cards = decks.rows[0].cards;
    if (order === -1){
      order = cards.length;
    } else{
      order -= 1;
    }

    await pool.query("BEGIN");
    try{
      const updatedCards = await pool.query(
        "UPDATE cards SET title = $1, note = $2 WHERE cid = $3 AND owner = $4 RETURNING *",
        [title, note, cid, did]
      );

      if (updatedCards.rows[0].length === 0) throw new Error("Card doesn't exist");
      
      arrayMove(cards, cards.indexOf(cid), order);
      await pool.query(
        "UPDATE decks\
        SET cards = $1\
        WHERE did = $2 AND owner = $3",
        [cards, did, uid]
      );

      pool.query("COMMIT");
      return res.status(200).json(updatedCards.rows[0]);
    }
    catch(err){
      pool.query("ROLLBACK");
      return res.status(500).send(err.message);
    }
  }
  catch (err){
    res.status(500).send(err.message);
  }
});


app.get("/user", authorize, async (req, res) => {
  try{
    const user = req.session.user;
    const users = await pool.query(
      "SELECT * FROM users WHERE uid = $1",
      [user.uid]
    );

    res.status(200).json(users.rows[0]);
  }
  catch (err){
    res.status(500).send(err.message);
  }
});


app.get("/deck/:did", authorize, async (req, res) => {
  try{
    const { uid } = req.session.user;
    const { did } = req.params;
    const userDecks = await pool.query(
      "SELECT * FROM decks WHERE did = $1 AND owner = $2",
      [did, uid]
    );

    if (userDecks.rows.length > 0){
      return res.status(200).json(userDecks.rows[0]);
    }
    else{
      return res.status(400).send("No such deck for this user");
    }
  }
  catch (err){
    res.status(500).send(err.message);
  }
});


app.get("/app", async (req, res) => {
  try{
    res.status(200).send(appUrl);
  }
  catch(err){
    res.status(500).send(err.message);
  }
})


app.get("/decks", authorize, async (req, res) => {
  try{
    const { uid } = req.session.user;
    const userDecks = await pool.query(
      "SELECT * FROM decks WHERE owner = $1",
      [uid]
    );

    res.status(200).json(userDecks.rows);
  }
  catch (err){
    res.status(500).send(err.message);
  }
});


app.get("/card/:cid", authorize, async (req, res) => {
  try{
    const { uid } = req.session.user;
    const { cid } = req.params;

    const cards = await pool.query(
      "SELECT * FROM cards WHERE cid = $1",
      [cid]
    );
    const decks = await pool.query(
      "SELECT * FROM decks WHERE did = $1 AND owner = $2",
      [cards.rows[0].owner, uid]
    );

    if (decks.rows.length > 0){
      return res.status(200).json(cards.rows[0]);
    }
    else{
      return res.status(400).send("No such card for this user");
    }
  }
  catch (err){
    res.status(500).send(err.message);
  }
});


app.get("/cards/:did", authorize, async (req, res) => {
  try{
    const { uid } = req.session.user;
    const { did } = req.params;

    const decks = await pool.query(
      "SELECT * FROM decks WHERE did = $1 AND owner = $2",
      [did, uid]
    );
    if (decks.rows.length > 0){
      const deckCards = await pool.query(
        "SELECT * FROM cards WHERE owner = $1",
        [did]
      );
      return res.status(200).json(deckCards.rows);
    } else{
      return res.status(401).send("Cannot display cards for this user");
    }

  }
  catch (err){
    res.status(500).send(err.message);
  }
});


app.get("/users", adminAuthorize, async (req, res) => {
  try{
    const users = await pool.query(
      "SELECT * FROM users"
    )

    res.status(200).json(users.rows)
  }
  catch(err){
    res.status(500).send(err.message)
  }
})


app.get("/decks-theme", adminAuthorize, async (req, res) => {
  try{
    const decksTheme = await pool.query(
      "SELECT theme FROM decks"
    );

    res.status(200).json(decksTheme.rows);
  }
  catch(err){
    res.status(500).send(err.message)
  }
})


app.get("/cards-count", adminAuthorize, async (req, res) => {
  try{
    const cards = await pool.query(
      "SELECT count (*) FROM cards"
    );

    res.status(200).json(cards.rows[0]);
  }
  catch(err){
    res.status(500).send(err.message)
  }
})


app.delete("/user", adminAuthorize, async (req, res) => {
  try{
    const { uid } = req.body;

    const users = await pool.query(
      "SELECT * FROM users WHERE uid = $1",
      [uid]
    );
    const decks = await pool.query(
      "SELECT * FROM decks WHERE owner = $1",
      [uid]
    );
    const dids = decks.rows.map(deck => deck.did);
    
    await pool.query("BEGIN");
    try{
      await pool.query(
        "DELETE FROM cards\
        WHERE owner = ANY($1)\
        RETURNING *",
        [dids]
      );

      await pool.query(
        "DELETE FROM decks\
        WHERE owner = $1\
        RETURNING *",
        [uid]
      );

      await pool.query(
        "DELETE FROM users\
        WHERE uid = $1\
        RETURNING *",
        [uid]
      );
    
      await sendEmail(users.rows[0].email, 'Account deleted', "Your quickcards account has been deleted");

      await pool.query("COMMIT");
      return res.status(200).send("User deleted successfully");
    }
    catch (err){
      await pool.query("ROLLBACK");
      return res.status(500).send(err.message);
    }
  }
  catch (err){
    res.status(500).send(err.message);
  }
})


app.delete("/deck", authorize, async (req, res) => {
  const { did } = req.body;

  try{
    const { uid } = req.session.user;

    await pool.query("BEGIN");
    try{
      const decks = await pool.query(
        "DELETE FROM decks\
        WHERE did = $1 AND owner = $2\
        RETURNING *",
        [did, uid]
      );

      if (decks.length === 0) throw new Error("Deck doesn't exist");

      await pool.query(
        "DELETE FROM cards\
        WHERE cid = ANY($1)\
        RETURNING *",
        [decks.rows[0].cards]
      );
      await pool.query(
        "UPDATE users\
        SET decks = ARRAY_REMOVE(decks, $1)\
        WHERE uid = $2",
        [did, uid]
      );
    
      await pool.query("COMMIT");
      return res.status(200).send(decks.rows[0]);
    }
    catch (err){
      await pool.query("ROLLBACK");
      return res.status(500).send(err.message);
    }
  }
  catch (err){
    res.status(500).send(err.message);
  }
});


app.delete("/card", authorize, async (req, res) => {
  const { cid, did } = req.body;

  try{
    const { uid } = req.session.user;

    await pool.query("BEGIN");
    try{
      const cards = await pool.query(
        "DELETE FROM cards\
        WHERE cid = $1 AND owner = $2\
        RETURNING *",
        [cid, did]
      );

      if (cards.length === 0) throw new Error("Card doesn't exist");

      await pool.query(
        "UPDATE decks\
        SET cards = ARRAY_REMOVE(cards, $1)\
        WHERE did = $2 AND owner = $3",
        [cid, did, uid]
      );
    
      await pool.query("COMMIT");
      return res.status(200).send(cards.rows[0]);
    }
    catch (err){
      await pool.query("ROLLBACK");
      return res.status(500).send(err.message);
    }
  }
  catch (err){
    res.status(500).send(err.message);
  }
});


app.delete("/sign-up-request", async (req, res) => {
  const { id, token } = req.body;

  try{
    await pool.query(
      "DELETE FROM sign_up_requests\
      WHERE id = $1 AND token = $2",
      [id, token]
    );

    res.status(200).send("Sign up request deleted");
  }
  catch (err){
    res.status(500).send(err.message);
  }
})


app.delete("/forgot-password-request", async (req, res) => {
  const { id, token } = req.body;

  try{
    await pool.query(
      "DELETE FROM forgot_password_requests\
      WHERE id = $1 AND token = $2",
      [id, token]
    );

    res.status(200).send("Forgot password request deleted");
  }
  catch (err){
    res.status(500).send(err.message);
  }
})


app.delete("/email-change-request", async (req, res) => {
  const { id, token } = req.body;

  try{
    await pool.query(
      "DELETE FROM email_change_requests\
      WHERE id = $1 AND token = $2",
      [id, token]
    );

    res.status(200).send("Email change request deleted");
  }
  catch (err){
    res.status(500).send(err.message);
  }
})


//VERB like apis
app.get("/is-verify", authorize, async (req, res) => {
  try{
    res.status(200).json(req.session.user);
  }
  catch(err){
    res.status(403).send(err.message);
  }
});


app.get("/is-admin-verify", adminAuthorize, async (req, res) => {
  try{
    res.status(200).json(req.session.admin);
  }
  catch(err){
    res.status(403).send(err.message);
  }
});


app.post("/sign-up", async (req, res) => {
  const { id, token } = req.body;

  try{
    const signUps = await pool.query(
      "SELECT * from sign_up_requests\
      WHERE id = $1",
      [id]
    );

    if (signUps.rows.length === 0){
      return res.status(400).send("Sign up request doesn't exist!");
    }

    const request= signUps.rows[0];
    if (request.token !== token){
      return res.status(401).send("Sign up request denied!");
    }

    var date= new Date();
    var requestDate= new Date(request.doc);
    if (diff_minutes(date, requestDate) > 60){
      return res.status(498).send("Sign up request expired!");
    }

    await pool.query("BEGIN");
    try{
      await pool.query(
        "DELETE FROM sign_up_requests\
        WHERE id = $1 AND token = $2\
        RETURNING *",
        [id, token]
      );

      var salt = bcrypt.genSaltSync(10);
      var hash = bcrypt.hashSync(request.user_password, salt);
      const newUsers = await pool.query(
        "INSERT INTO users(name, email, password, decks)\
        VALUES ($1, $2, $3, $4)\
        RETURNING *",
        [request.user_name, request.user_email, hash, []]
      );

      const sampleDecks = await pool.query(
        "INSERT INTO decks(name, theme, cards, owner)\
        VALUES ('Sample Deck Sky', 'sky', $1, $2),\
        ('Sample Deck Salmon', 'salmon', $1, $2),\
        ('Sample Deck Default', 'default', $1, $2)\
        RETURNING *",
        [[], newUsers.rows[0].uid]
      );

      await pool.query("COMMIT");
      return res.status(200).json({email: request.user_email, password: request.user_password});
    }
    catch (err){
      await pool.query("ROLLBACK");
      return res.status(500).send(err.message);
    }
  }
  catch (err){
    res.status(500).send(err.message);
  }
});


app.post("/sign-in", validateUserInfo, authuenticate, async (req, res) => {
  const { email } = req.body;

  try{
    const users = await pool.query(
      "SELECT * FROM users WHERE email = $1",
      [email]
    );
    const { uid, name } = users.rows[0];

    req.session.regenerate(() => {
      req.session.user = { uid };
      res.status(200).send("Successful login");
    });
  }
  catch (err){
    res.status(500).send(err.message);
  }
});


app.post("/admin-sign-in", validateUserInfo, async (req, res) => {
  const { password } = req.body;

  try{
    if (password !== adminPassword){
      return res.status(401).json({ input: "password", feedback: "Invalid credential" });
    }

    req.session.regenerate(() => {
      req.session.admin = true;
      res.status(200).send("Successful login");
    });
  }
  catch (err){
    res.status(500).send(err.message);
  }
});


app.delete("/sign-out", async (req, res) => {
  if(req.session.user){
    req.session.destroy(() => {
      res.status(200).send("User Logged out")
    })
  }
  else{
    res.status(400).send("No user to log out");
  }
});


app.delete("/admin-sign-out", async (req, res) => {
  if(req.session.admin){
    req.session.destroy(() => {
      res.status(200).send("User Logged out")
    })
  }
  else{
    res.status(400).send("No user to log out");
  }
});


app.post("/forgot-password", async (req, res) => {
  const { id, token } = req.body;

  try{
    const signUps = await pool.query(
      "SELECT * from forgot_password_requests\
      WHERE id = $1",
      [id]
    );

    if (signUps.rows.length === 0){
      return res.status(400).send("Forgot password request doesn't exist!");
    }

    const request= signUps.rows[0];
    if (request.token !== token){
      return res.status(401).send("Forgot password request denied!");
    }

    var date= new Date();
    var requestDate= new Date(request.doc);
    if (diff_minutes(date, requestDate) > 60){
      return res.status(498).send("Forgot password request expired!");
    }

    await pool.query("BEGIN");
    try{
      await pool.query(
        "DELETE FROM forgot_password_requests\
        WHERE id = $1 AND token = $2\
        RETURNING *",
        [id, token]
      );

      var salt = bcrypt.genSaltSync(10);
      var hash = bcrypt.hashSync(request.user_password, salt);
      const updatedUsers = await pool.query(
        "UPDATE users\
        SET password = $1 WHERE email = $2\
        RETURNING *",
        [hash, request.user_email]
      );

      if (updatedUsers.rows.length === 0){
        await pool.query("COMMIT");
        return res.status(405).send("Password reset failed!");
      }

      await pool.query("COMMIT");
      return res.status(200).json({email: request.user_email, password: request.user_password});
    }
    catch (err){
      await pool.query("ROLLBACK");
      return res.status(500).send(err.message);
    }
  }
  catch (err){
    res.status(500).send(err.message);
  }
});


app.post("/name-validate-only", validateUserInfo, async(req, res) => {
  res.status(200).send("Valid password");
});


app.post("/email-validate-only", validateUserInfo, restrictEmailUse, async(req, res) => {
  res.status(200).send("Valid email");
});


app.post("/password-validate-only", validateUserInfo, async(req, res) => {
  res.status(200).send("Valid input");
});


app.post("/change-email", async (req, res) => {
  const { id, token } = req.body;

  try{
    const requests = await pool.query(
      "SELECT * from email_change_requests\
      WHERE id = $1",
      [id]
    );

    if (requests.rows.length === 0){
      return res.status(400).send("Email change request doesn't exist!");
    }

    const request= requests.rows[0];
    if (request.token !== token){
      return res.status(401).send("Email change request denied!");
    }

    var date= new Date();
    var requestDate= new Date(request.doc);
    if (diff_minutes(date, requestDate) > 60){
      return res.status(498).send("Email change request expired!");
    }

    await pool.query("BEGIN");
    try{
      await pool.query(
        "DELETE FROM email_change_requests\
        WHERE id = $1 AND token = $2\
        RETURNING *",
        [id, token]
      );

      await pool.query(
        "UPDATE users SET email = $1 WHERE uid = $2",
        [request.user_email, request.user_uid]
      );

      await pool.query("COMMIT");
      return res.status(200).json("Email changed successfully");
    }
    catch (err){
      await pool.query("ROLLBACK");
      return res.status(500).send(err.message);
    }
  }
  catch (err){
    res.status(500).send(err.message);
  }
});


//server listen
app.listen(PORT, () => {
  console.log(`Server has started on port ${PORT}`);
});