export default function Logo(){
  return (
    <div style={{width : '80px'}}>
      <img src={process.env.PUBLIC_URL + '/Quickcards_logo_title.png'} className="img-fluid" alt="logo"/>
    </div>
  );
}

export function NavLogo(){
  return (
    <a href="/">
      <div className="d-inline-block" style={{width : '40px'}}>
        <img src={process.env.PUBLIC_URL + '/Quickcards_logo.png'} className="img-fluid" alt="logo"/>
      </div>
    </a>
  );
}