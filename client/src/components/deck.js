export default function Deck({name, theme, size, visible, onClick, onInfo, onEdit, onDelete}){
  const ratio = 100/75;
  const width = 100 + (size * 60);
  const height = width / ratio;
  const themeClass= "q-item q-item-" + theme;
  const disabled= visible ? "" : " disabled";

  return (
    <div onClick={e => e.stopPropagation()} className={themeClass + " text-center fs-" + (7 - size)}>
      <div className="d-flex justify-content-center align-items-center border border-2 text-break" style={{width, height}} onClick={onClick}>
        {name}
      </div>
      <div className={"q-mark" + (visible ? "" : " q-invisible")}>
        <div className="d-flex justify-content-around align-items-center">  
          <button className={disabled} onClick={onInfo}>
            <i className="bi bi-info-circle" style={{fontSize: '1.2rem'}}></i>
          </button>
          <button className={disabled} onClick={onEdit}>
            <i className="bi bi-pen" style={{fontSize: '1.2rem'}}></i>
          </button>
          <button className={disabled} onClick={onDelete}>
            <i className="bi bi-trash" style={{fontSize: '1.2rem'}}></i>
          </button>
        </div>
      </div>
    </div>
  );
}
