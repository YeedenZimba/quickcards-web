export default function Empty(){
  return (
    <div style={{width : '100px'}}>
      <img src={process.env.PUBLIC_URL + '/empty.png'} className="img-fluid" alt="logo"/>
    </div>
  );
}