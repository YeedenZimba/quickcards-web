import React, { useState, useEffect, useContext, useRef } from "react";
import { useNavigate } from "react-router-dom";
import { Modal } from "bootstrap";

import SideBar from "../components/sideBar";
import AdminContext from "../contexts/adminContext";

import { uri } from "../globals";

export default function AdminUsers(){
  const STATUS={
    typing : 0,
    submit : 1
  }
  const navigate = useNavigate();
  const [status, setStatus] = useState(STATUS.typing);
  const {users, setUsers} = useContext(AdminContext);
  const resultModal = useRef(null);
  const [modalData, setModalData] = useState({title: "", body: "", delete: false, uid: ""});

  async function deleteUser(uid){
    setStatus(STATUS.submit)
    try{
      const res = await fetch(`${uri}/user`, {
        method: "DELETE",
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({uid})
      })

      if (res.ok) {
        setModalData({title: "Success", body: <span>User has been deleted</span>, delete: false})

        const newUsers = users;
        delete newUsers[uid];
        setUsers(newUsers);
      };
    }
    catch (err){
      setModalData({title: "Error", body: "There was a problem when deleting the user", delete: false})
    }
    setStatus(STATUS.typing);
  }

  useEffect(() => {
    let ignore = false;
    var modal = new Modal(document.getElementById('resultModal'));
    resultModal.current = modal;

    fetch(`${uri}/users`, {
      method: "GET",
      credentials: "include"
    })
    .then(res => {
      if(res.ok){
        res.json().then(json => {
          const newUsers = {}
          json.forEach(user => {
            const { uid, ...others } = user;
            newUsers[uid] = others; 
          })
          setUsers(newUsers)
        })  
      } else if(!ignore && res.status === 403){
        navigate('/');
      }
    });
    
    return () => {ignore = true}
  }, []);

  function openModal(){
    resultModal.current.show();
  }

  function closeModal(){
    resultModal.current.hide();
  }


  const body=(
    <div>
      <div className="row g-0">
        <div className="col-12 col-lg-1 border d-flex align-items-center justify-content-center bg-light" style={{minHeight: 60}}>
          <span></span>
        </div>
        <div className="col-12 col-lg-3 border d-flex align-items-center" style={{minHeight: 60}}>
          <b>Name</b>
        </div>
        <div className="col-12 col-lg-4 border d-flex align-items-center" style={{minHeight: 60}}>
          <b>Email</b>
        </div>
        <div className="col-12 col-lg-3 border d-flex align-items-center" style={{minHeight: 60}}>
          <b>Created at</b>
        </div>
        <div className="col-12 col-lg-1 border d-flex align-items-center justify-content-center" style={{minHeight: 60}}>
        </div>
      </div>


      {
        Object.keys(users).map((uid, index) => {
          const {name, email, doc} = users[uid];
          const date = new Date(doc).toDateString();
          return <div 
            key={uid} 
            className="row mb-0 g-0"
          >
            <div className="col-12 col-lg-1 border d-flex align-items-center justify-content-center bg-light" style={{minHeight: 60}}>
              <span>{index + 1}</span>
            </div>
            <div className="col-12 col-lg-3 border d-flex align-items-center" style={{minHeight: 60}}>
                <i class="bi bi-person-circle "></i>
                <span>{name}</span>
            </div>
            <div className="col-12 col-lg-4 border d-flex align-items-center" style={{minHeight: 60}}>
              <i class="bi bi-envelope"></i>
              {email}
            </div>
            <div className="col-12 col-lg-3 border d-flex align-items-center" style={{minHeight: 60}}>
              <i class="bi bi-calendar-event"></i>
              {date}
            </div>
            <div className="col-12 col-lg-1 border d-flex align-items-center justify-content-center" style={{minHeight: 60}}>
              <button className="btn" onClick={() => {
                setModalData({title: "Delete user", body: `Are you sure want to delete user ${name}?`, delete: true, uid});
                openModal();
              }} title="Delete" disabled={status === STATUS.submit}>
                <i className="bi bi-trash" style={{fontSize: '1.2rem'}}></i>
              </button>
            </div>

          </div>
        })
      }
      <div className="modal" id="resultModal" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">
                {modalData.title}
              </h5>
              <button type="button" className="btn-close" onClick={closeModal} disabled={status === STATUS.submit}></button>
            </div>
            <div className="modal-body">
              {modalData.body}
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" onClick={closeModal} disabled={status === STATUS.submit}>Close</button>
              {modalData.delete && <button type="button" className="btn btn-danger" onClick={() => deleteUser(modalData.uid)} disabled={status === STATUS.submit}>Delete</button>}
            </div>
            {status === STATUS.submit && <div className="spinner-border text-primary d-block mx-auto mb-3" role="status"/>}
          </div>
        </div>
      </div>
    </div>
  );
  
  return (
    <SideBar nav="users" body={body} title="Users"/>
  );
}