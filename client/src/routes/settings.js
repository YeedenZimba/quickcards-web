import React, { useState, useEffect, useRef } from "react";
import { useNavigate } from "react-router-dom";
import { NavLogo } from "../components/logo";
import { Modal } from 'bootstrap';

import { uri } from "../globals";

export default function Settings(){
  const STATUS={
    typing : 0,
    submit : 1
  }
  const navigate = useNavigate();

  const [data, setData] = useState({name: "", email: "", password: "", confirmPassword: ""});
  const [modalData, setModalData] = useState({title: "", body: ""});
  const [userInfo, setUserInfo] = useState(null);
  const [password, setPassword] = useState("");
  const [status, setStatus] = useState(STATUS.typing);
  const [invalidFeedback, setInvalidFeedback] = useState({input: "", feedback: ""});
  const [currentEdit, setCurrentEdit] = useState("");
  const reviewModal = useRef(null);

  useEffect(() => {
    let ignore = false;
    var modal = new Modal(document.getElementById('reviewModal'));
    reviewModal.current = modal;

    fetch(`${uri}/user`, {
      method: "GET",
      credentials: "include"
    })
    .then(res => {
      if (!ignore){
        if (res.ok){
          res.json()
          .then(json => {
            setUserInfo({...json, doc: new Date(json.doc)});
          })
        }
        else{
          navigate('/sign-in');
        }
      }
    })
    return () => {ignore = true}
  }, []);

  async function signOut(){
    try{
      const res = await fetch(`${uri}/sign-out`, {
        method: "DELETE",
        credentials: "include"
      });

      navigate('/sign-in');
    }
    catch(err){
      navigate('/error/' + err.message);
    }
  }

  async function review(type){
    const filteredData = {
      name: data.name.trim(),
      email: data.email.trim(),
      password: data.password.trim(),
      confirmPassword: data.confirmPassword.trim() 
    }
    setData(filteredData)

    setStatus(STATUS.submit);
    try{
      const res = await fetch(`${uri}/${type}-validate-only`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(filteredData)
      });

      if (res.ok){
        setInvalidFeedback({input: "", feedback: ""});
        setStatus(STATUS.typing);
        if(type === "name"){
          setModalData({title: "Authentiation required!", body: <span>Enter password to change your name from <b>{userInfo.name}</b> to <b>{filteredData.name}</b></span>});
        } else if(type === "email"){
          setModalData({title: "Authentiation required!", body: "Enter your password to proceed"});
        } else if(type === "password"){
          setModalData({title: "Authentiation required!", body: "Enter old password"});
        }
        reviewModal.current.show();
      }
      else{
        res.json().then(json => {
          if (json.input && json.feedback){
            setStatus(STATUS.typing);
            setInvalidFeedback(json);
          }
        });
      }
    }
    catch (err){
      setStatus(STATUS.typing);
      setInvalidFeedback({input: "", feedback: ""});
    }
  }

  async function update(){
    setStatus(STATUS.submit);
    try{
      if (currentEdit === "email"){
        const res = await fetch(`${uri}/email-change-request`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({...userInfo, newEmail: data.email, password: password})
        });
  
        if(res.ok){
          setModalData({title: "Requested succesfully", body: <span>An email has been sent to <b>{data.email}</b></span>});
          setData({...data, email: ""});
          setInvalidFeedback({input: "", feedback: ""});
          setCurrentEdit("");
        } else if(res.status === 401){
          res.json().then(json => {
            if (json.input && json.feedback){
              setInvalidFeedback(json);
              setStatus(STATUS.typing);
              setPassword("");
            }
          });
        }
        else{
          setModalData({title: "Request failed", body: <span>Please try again later</span>});
          setInvalidFeedback({input: "", feedback: ""});
          setCurrentEdit("");
        }

        setStatus(STATUS.typing);
      } else{
        let body = {}
        if (currentEdit === "name"){
          body = {...userInfo, name: data.name, password: password}
        } else if (currentEdit === "password"){
          body = {...userInfo, newPassword: data.password, password: password}
        }
        
        const res = await fetch(`${uri}/${currentEdit}`, {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(body)
        });

        if (res.ok){
          if (currentEdit === "name"){
            setModalData({title: "Success", body: <span>Your name has been changed to <b>{data.name}</b></span>});
            setData({...data, name: ""});
            setUserInfo({...userInfo, name: data.name})
          } else if (currentEdit === "password"){
            setData({...data, password: "", confirmPassword: ""});
            setModalData({title: "Success", body: "Your password has been changed"});
          }

          setInvalidFeedback({input: "", feedback: ""});
          setStatus(STATUS.typing);
          setCurrentEdit("");
        }
        else{
          res.json().then(json => {
            if (json.input && json.feedback){
              setInvalidFeedback(json);
              setStatus(STATUS.typing);
              setPassword("");
            }
          });
        }
      }
    }
    catch (err){
      setStatus(STATUS.typing);
      setInvalidFeedback({input: "", feedback: ""});
    }
  }

  function closeModal(){
    setPassword("");
    reviewModal.current.hide();
  }

  return (
    <div>
      <nav className="navbar fixed-top navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid ">
          <a className="navbar-brand">
            <NavLogo/>
            <span className="navbar-text ms-2">
              QUICKCARDS
            </span>
          </a>
          <button className="btn" onClick={() => navigate(-1)}>
            <i className="bi bi-x-lg" style={{fontSize: '1.2rem'}}></i>
          </button>
        </div>
      </nav>

      <div className="container-fluid">
        <div className="col-11 col-sm-8 mx-auto mt-5 pt-5" style={{maxWidth: '600px'}}>
          <div className="mb-4">
            <h3>User settings</h3>
          </div>
          <form className="mb-4">
            <div className="row">
              <div className="col-3 col-sm-2 text-break">
                Name
              </div>
              <div className="col-7 col-sm-8 text-break">
                {userInfo && userInfo.name}
              </div>
              <div className="col-2">
                {currentEdit !== "name" && <a className="text-decoration-none text-primary" onClick={() => setCurrentEdit("name")}>Edit</a>}
              </div>
            </div>
            {currentEdit === "name" &&
              <div className="row mt-3">
                <div className="col-sm-2" />
                <div className="col-sm">
                  <div className="input-group mb-3">
                    <div className="input-group-text">New name</div>
                    <input type="text" className={"form-control" + (invalidFeedback.input === "name" ? " is-invalid" : "")} aria-describedby="emailFeedback" value={data.name}
                      onChange={(e) => {
                        setData({...data, name : e.target.value});
                      }}
                    />
                    <div id="emailFeedback" className="invalid-feedback">{invalidFeedback.feedback}</div>
                  </div>
                  <div className="text-secondary mb-3">
                    Name should have a maximum of 30 aplhanumeric characters including spaces.
                  </div>
                  <div className="d-flex">
                    <button type="button" className="btn btn-primary me-2" onClick={() => review("name")} disabled={status === STATUS.submit}>Review change</button>
                    <button type="button" className="btn btn-outline-primary" onClick={() => setCurrentEdit("")} disabled={status === STATUS.submit}>Cancel</button>
                  </div>
                </div>
              </div>
            }
          </form>
          <form className="mb-4">
            <div className="row">
              <div className="col-3 col-sm-2 text-break">
                Email
              </div>
              <div className="col-7 col-sm-8 text-break">
                {userInfo && userInfo.email}
              </div>
              <div className="col-2">
                {currentEdit !== "email" && <a className="text-decoration-none text-primary" onClick={() => setCurrentEdit("email")}>Edit</a>}
              </div>
            </div>
            {currentEdit === "email" &&
              <div className="row mt-3">
                <div className="col-sm-2"/>
                <div className="col-sm">
                  <div className="input-group mb-3">
                    <div className="input-group-text">New Email</div>
                    <input type="text" className={"form-control" + (invalidFeedback.input === "email" ? " is-invalid" : "")} placeholder="email@example.com" aria-describedby="emailFeedback" value={data.email}
                      onChange={(e) => {
                        setData({...data, email : e.target.value});
                      }}
                    />
                    <div id="emailFeedback" className="invalid-feedback">{invalidFeedback.feedback}</div>
                  </div>
                  <div className="text-secondary mb-3">
                    A link would be sent to the new email for authentication purposes.
                  </div>
                  <div className="d-flex">
                    <button type="button" className="btn btn-primary me-2" onClick={() => review("email")} disabled={status === STATUS.submit}>Review change</button>
                    <button type="button" className="btn btn-outline-primary" onClick={() => setCurrentEdit("")} disabled={status === STATUS.submit}>Cancel</button>
                  </div>
                </div>
              </div>
            }
          </form>
          <form className="mb-4">
            <div className="row">
              <div className="col-3 col-sm-2 text-break">
                Password
              </div>
              <div className="col-7 col-sm-8">
                {currentEdit !== "password" && "(Click on edit to change password)"}
              </div>
              <div className="col-2">
                {currentEdit !== "password" && <a className="text-decoration-none text-primary" onClick={() => setCurrentEdit("password")}>Edit</a>}
              </div>
            </div>
            {currentEdit === "password" &&
              <div className="row mt-3">
                <div className="col-sm-2"/>
                <div className="col-sm">
                  <div className="input-group mb-3">
                    <div className="input-group-text" style={{width: '155px'}}>New password</div>
                    <input type="password" className={"form-control" + (invalidFeedback.input === "newPassword" ? " is-invalid" : "")} aria-describedby="emailFeedback" value={data.password}
                      onChange={(e) => {
                        setData({...data, password : e.target.value});
                      }}
                    />
                    <div id="emailFeedback" className="invalid-feedback">{invalidFeedback.feedback}</div>
                  </div>
                  <div className="input-group mb-3">
                    <div className="input-group-text"  style={{width: '155px'}}>Confirm password</div>
                    <input type="password" className={"form-control" + (invalidFeedback.input === "confirmPassword" ? " is-invalid" : "")} aria-describedby="emailFeedback" value={data.confirmPassword}
                      onChange={(e) => {
                        setData({...data, confirmPassword : e.target.value});
                      }}
                    />
                    <div id="emailFeedback" className="invalid-feedback">{invalidFeedback.feedback}</div>
                  </div>
                  <div className="text-secondary mb-3">
                    Password should have atleast 8 characters.
                  </div>
                  <div className="d-flex">
                    <button type="button" className="btn btn-primary me-2" disabled={status === STATUS.submit} onClick={() => review("password")}>Review change</button>
                    <button type="button" className="btn btn-outline-primary" onClick={() => setCurrentEdit("")} disabled={status === STATUS.submit}>Cancel</button>
                  </div>
                </div>
              </div>
            }
          </form>
          <div className="row mb-4">
            <div className="col-3 col-sm-2 text-break">
              First seen
            </div>
            <div className="col-7 col-sm-8">
              {userInfo && userInfo.doc.toDateString()}
            </div>
          </div>
          <div className="row mb-4">
            <div className="col-3 col-sm-2 text-break">
              Total deck
            </div>
            <div className="col-7 col-sm-8">
              {userInfo && userInfo.decks.length}
            </div>
          </div>
          <button type="button" className="btn btn-secondary btn-sm" onClick={signOut}>Log out</button>
        </div>
      </div>

      <div className="modal" id="reviewModal" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">
                {modalData.title}
              </h5>
              <button type="button" className="btn-close" onClick={closeModal}></button>
            </div>
            <div className="modal-body">
              {modalData.body}
              {currentEdit &&
                <div className="input-group mt-3">
                  <div className="input-group-text">Password</div>
                  <input type="password" className={"form-control" + (invalidFeedback.input === "password" ? " is-invalid" : "")} aria-describedby="emailFeedback" value={password}
                    onChange={(e) => {
                      setPassword(e.target.value);
                    }}
                  />
                  <div id="emailFeedback" className="invalid-feedback">{invalidFeedback.feedback}</div>
                </div>
              }
            </div>

            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" onClick={closeModal} disabled={status === STATUS.submit}>Close</button>
              {currentEdit && <button type="button" className="btn btn-primary" onClick={update} disabled={status === STATUS.submit}>Update</button>}
            </div>
            {status === STATUS.submit && <div className="spinner-border text-primary d-block mx-auto mb-3" role="status"/>}
          </div>
        </div>
      </div>
    </div>
  );
}