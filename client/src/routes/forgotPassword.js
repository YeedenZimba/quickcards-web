import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom"
import Logo from "../components/logo";
import PageAlert from "../components/pageAlert";

import { uri } from "../globals";

export default function ForgotPassword(){
  const STATUS={
    typing : 0,
    submit : 1
  }

  const navigate = useNavigate();
  const [data, setData] = useState({email: "", password: "", confirmPassword: ""});
  const [status, setStatus] = useState(STATUS.typing);
  const [emailSent, setEmailSent] = useState(false);
  const [invalidFeedback, setInvalidFeedback] = useState({input: "", feedback: ""});

  useEffect(() => {
    let ignore = false;
    fetch(`${uri}/is-verify`, {
      method: "GET",
      credentials: "include"
    })
    .then(res => res.ok && !ignore && navigate('/home'))
    return () => {ignore = true}
  }, []);

  async function onSubmit(event){
    event.preventDefault();
    const filteredData = {
      email: data.email.trim(),
      password: data.password.trim(),
      confirmPassword: data.confirmPassword.trim()
    }
    setData(filteredData);
    setStatus(STATUS.submit);
    setInvalidFeedback({input: "", feedback: ""});
    try{
      const res = await fetch(`${uri}/forgot-password-request`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(filteredData)
      });

      if(res.status === 400){
        res.json().then(json => {
          if (json.input && json.feedback){
            setInvalidFeedback(json);
            setStatus(STATUS.typing);
          }
        });
      } else if(res.status === 500){
        navigate("/error/server problem");
      }
      else{
        setInvalidFeedback({input: "", feedback: ""});
        setEmailSent(true);
      }
    }
    catch (err){
      navigate('/error/' + err.message);
    }
  }

  return (
    !emailSent ?
      <div className="d-flex flex-column vh-100 justify-content-center align-items-center">
        <Logo />
        <h3>Change password</h3>
        <div className="container-fluid">
          <div className="row">
            <div className="col col-sm-7 mx-auto" style={{maxWidth: '450px'}}>
              <form onSubmit={onSubmit}>
                <div className="mb-3">
                  <label className="form-label">Email</label>
                  <input type="text" className={"form-control" + (invalidFeedback.input === "email" ? " is-invalid" : "")} placeholder="email@example.com" aria-describedby="emailFeedback" value={data.email}
                    onChange={(e) => {
                      setData({...data, email : e.target.value});
                    }}
                  />
                  <div id="emailFeedback" className="invalid-feedback">{invalidFeedback.feedback}</div>
                </div>
                <div className="mb-3">
                  <label className="form-label">New password</label>
                  <input type="password" className={"form-control" + (invalidFeedback.input === "password" ? " is-invalid" : "")} aria-describedby="passwordFeedback" value={data.password} autoComplete="new-password"
                    onChange={(e) => {
                      setData({...data, password : e.target.value});
                    }}
                  />
                  <div id="passwordFeedback" className="invalid-feedback">{invalidFeedback.feedback}</div>
                </div>
                <div className="mb-3">
                  <label className="form-label">Confirm password</label>
                  <input type="password" className={"form-control" + (invalidFeedback.input === "confirmPassword" ? " is-invalid" : "")} aria-describedby="passwordFeedback" value={data.confirmPassword}
                    onChange={(e) => {
                      setData({...data, confirmPassword : e.target.value});
                    }}
                  />
                  <div id="passwordFeedback" className="invalid-feedback">{invalidFeedback.feedback}</div>
                </div>
                {status === STATUS.submit && <div className="spinner-border text-primary d-block mx-auto mb-3" role="status"/>}
                <button type="submit" className="btn btn-primary d-block mx-auto" disabled={status === STATUS.submit}>NEXT</button>
                <button type="button" className="btn btn-link d-block mx-auto" disabled={status === STATUS.submit} onClick={() => navigate('/sign-in')}>move to sign in</button>
              </form>
            </div>
          </div>
        </div>
      </div> :
    <PageAlert type="secondary" text={<span>An email has been sent to <b>{data.email}</b></span>} loading={false}/>
  );
}