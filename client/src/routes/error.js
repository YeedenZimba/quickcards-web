import React, { useState } from "react";
import { useParams } from "react-router-dom";
import PageAlert from "../components/pageAlert";


export default function Error(){
  const { message }= useParams();

  return (
    <PageAlert type="danger" text={"Error! " + message + ". Please try again later"} loading={false}/>
  );
}