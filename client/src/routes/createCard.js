import React, { useState, useEffect, useRef } from "react";
import { useNavigate } from "react-router-dom";
import { NavLogo } from "../components/logo";
import { Modal } from 'bootstrap';
import { useParams } from "react-router-dom";

import { uri } from "../globals";


export default function CreateCard(){
  const STATUS={
    typing : 0,
    submit : 1
  }
  const navigate = useNavigate();
  const { did }= useParams();

  const [data, setData] = useState({title: "", note: "", order: 1});
  const [currentDeck, setCurrentDeck] = useState({deckName: "", cards: []});
  const [status, setStatus] = useState(STATUS.typing);
  const [invalidFeedback, setInvalidFeedback] = useState({input: "", feedback: ""});
  const [orderType, setOrderType] = useState("first");
  const [modalData, setModalData] = useState({title: "", body: ""});
  const resultModal = useRef(null);
  const maxOrder = useRef(1);
  

  useEffect(() => {
    let ignore = false;

    var modal = new Modal(document.getElementById('resultModal'));
    resultModal.current = modal;

    fetch(`${uri}/deck/${did}`, {
      method: "GET",
      credentials: "include"
    })
    .then(res => {
      if (!ignore){
        if (res.ok){
          res.json()
          .then(json => {
            setCurrentDeck({deckName: json.name, cards: json.cards});
            maxOrder.current = json.cards.length + 1;
          })
        } else if (res.status === 200){
          navigate('/sign-in');
        } else{
          navigate('/error/' + res.text());
        }
      }
    })
    return () => {ignore = true};
  }, []);

  function openModal(){
    resultModal.current.show();
  }

  function closeModal(){
    resultModal.current.hide();
    navigate("/cards/" + did);
  }

  async function onSubmit(event){
    event.preventDefault();
    const filteredData = {...data, title: data.title.trim(), note: data.note.trim()}
    setData(filteredData);
    setStatus(STATUS.submit);

    try{
      const res = await fetch(`${uri}/card`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        credentials: "include",
        body: JSON.stringify({...filteredData, did})
      });

      if(!res.ok){
        if (res.status === 403){
          setModalData({title: "Failed!", body: "Failed to authorize user."});
          openModal();
        } else{
          res.json().then(json => {
            if (json.input && json.feedback){
              setInvalidFeedback(json);
              setStatus(STATUS.typing);
            }
          });
        }
      }
      else{
        setInvalidFeedback({input: "", feedback: ""});
        setModalData({title: "Congratulations!", body: <span>You have created a new card called <b>{data.title}</b></span>});
        openModal();
      }
    }
    catch (err){
      navigate('/error/' + err.message);
    }
  }

  return (
    <div>
      <nav className="navbar fixed-top navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid ">
          <a className="navbar-brand">
            <NavLogo/>
            <span className="navbar-text ms-2">
              QUICKCARDS
            </span>
          </a>
          <button className="btn" onClick={() => navigate('/cards/' + did)}>
            <i className="bi bi-x-lg" style={{fontSize: '1.2rem'}}></i>
          </button>
        </div>
      </nav>
      <div className="container-fluid">
        <div className="d-flex flex-column mt-5 pt-5 justify-content-center align-items-center">
        <h3 className="mb-3">Create a new card</h3>
        <div className="row w-100">
          <div className="col col-sm-7 mx-auto" style={{maxWidth: '450px'}}>
            <form onSubmit={onSubmit}>
              <div className="mb-3">
                <label className="form-label">Title</label>
                <input type="text" className={"form-control" + (invalidFeedback.input === "title" ? " is-invalid" : "")} placeholder="Enter card title" aria-describedby="emailFeedback" autoComplete="off" value={data.title}
                  onChange={e => {
                    setData({...data, title: e.target.value});
                  }}
                />
                <div id="emailFeedback" className="invalid-feedback">{invalidFeedback.feedback}</div>
              </div>
              <div className="form-floating mb-3">
                <textarea className={"form-control" + (invalidFeedback.input === "note" ? " is-invalid" : "")} placeholder="Leave a comment here" aria-describedby="noteFeedback" id="floatingTextarea2" style={{height: '200px'}}
                  onChange={e => {
                    setData({...data, note: e.target.value});
                  }}
                />
                <label htmlFor="floatingTextarea2">Note</label>
                <div id="noteFeedback" className="invalid-feedback">{invalidFeedback.feedback}</div>
              </div>
              <div className="mb-3">
                <label className="form-label">Pick a card order</label>
                <select className="form-select" aria-label="Default select example" defaultValue="first"
                  onChange={e => {
                    setOrderType(e.target.value);
                    if (e.target.value === "last"){
                      setData({...data, order: -1});
                    } else{
                      setData({...data, order: 1});
                    }
                  }}
                >
                  <option value="first">First</option>
                  <option value="last">Last</option>
                  <option value="between">Between</option>
                </select>
                <div id="passwordFeedback" className="invalid-feedback">{invalidFeedback.feedback}</div>
              </div>
              {orderType === "between" &&
                <div className="mb-3">
                  <label htmlFor="customRange3" className="form-label">Select card position</label>
                  <input type="range" className="form-range" min="1" max={maxOrder.current} step="1" id="customRange3" value={data.order}
                    onChange={e => {
                      setData({...data, order: e.target.value});
                    }}
                  />
                  <input type="number" className="form-control" placeholder="Enter card title" aria-describedby="emailFeedback" autoComplete="off" value={data.order}
                    onChange={e => {
                      let n = e.target.value;
                      n = Math.min(maxOrder.current, n);
                      n = Math.max(1, n);
                      setData({...data, order: n});
                    }}
                  />
                </div>
              }
              
              <button type="submit" className="btn btn-primary d-block mx-auto" disabled={status === STATUS.submit}>CREATE CARD</button>
            </form>
          </div>
        </div>
        </div>
      </div>
      <div className="modal" id="resultModal" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">
                {modalData.title}
              </h5>
              <button type="button" className="btn-close" onClick={closeModal}></button>
            </div>
            <div className="modal-body">
              {modalData.body}
            </div>

            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" onClick={closeModal}>Close</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}