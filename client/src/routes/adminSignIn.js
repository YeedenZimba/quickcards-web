import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom"
import { NavLogo } from "../components/logo";
import { uri } from "../globals";

export default function AdminSignIn(){
  const STATUS={
    typing : 0,
    submit : 1
  }

  const navigate = useNavigate();
  const [data, setData] = useState({password: ""});
  const [status, setStatus] = useState(STATUS.typing);
  const [invalidFeedback, setInvalidFeedback] = useState({input: "", feedback: ""});

  useEffect(() => {
    let ignore = false;
    fetch(`${uri}/is-admin-verify`, {
      method: "GET",
      credentials: "include"
    })
    .then(res => res.ok && !ignore && navigate('/admin-statistics'));
    return () => {ignore = true}
  }, []);

  async function onSubmit(event){
    event.preventDefault();
    const filteredData = {
      password: data.password.trim()
    }
    setData(filteredData)
    setStatus(STATUS.submit);

    try{
      const res = await fetch(`${uri}/admin-sign-in`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        credentials: "include",
        body: JSON.stringify(filteredData)
      });
      if(!res.ok){
        res.json().then(json => {
          if (json.input && json.feedback){
            setInvalidFeedback(json);

            setStatus(STATUS.typing);
          }
        });
      }
      else{
        setInvalidFeedback({input: "", feedback: ""});
        navigate('/admin-statistics');
      }
    }
    catch (err){
      navigate('/error/' + err.message);
    }
  }
  
  return (
    <div className="d-flex flex-column vh-100 justify-content-center align-items-center">
      <div className="container-fluid">
        <div className="row">
          <div className="col col-sm-7 mx-auto border border-2 py-3 rounded" style={{maxWidth: '450px'}}>
            <div className="d-flex justify-content-between align-items-center mb-3">
              <h3>Sign in</h3>
              <NavLogo /> 
            </div>
            <div className="mb-3">Enter your admin credentials</div>
            <form onSubmit={onSubmit}>
              <div className="mb-3">
                <input placeholder="Password" type="password" className={"form-control" + (invalidFeedback.input === "password" ? " is-invalid" : "")} aria-describedby="passwordFeedback" autoComplete="new-password" value={data.password}
                  onChange={(e) => {
                    setData({...data, password : e.target.value});
                  }}
                />
                <div id="passwordFeedback" className="invalid-feedback">{invalidFeedback.feedback}</div>
              </div>
              {status === STATUS.submit && <div className="spinner-border text-primary d-block mx-auto mb-3" role="status"/>}
              <button type="submit" className="btn btn-primary w-100" disabled={status === STATUS.submit}>Authenticate</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}